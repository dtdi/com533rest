<?php 
    require 'Slim/Slim.php';
    \Slim\Slim::registerAutoloader();
    use Slim\Slim;
    $app = new Slim();
    $app->get('/staffs', 'getStaffs');
	$app->post('/staffs', 'addStaff');
	$app->get('/staffs/:id', 'getStaff');
	$app->put('/staffs/:id', 'updateStaff');
	$app->delete('/staffs/:id', 'deleteStaff');
    $app->run();
    
    function getStaffs() {
        $sql = "select * FROM staff ORDER BY Staff_Id";
		try {
			$db = getConnection();
			$stmt = $db->query($sql);
			$staffs = $stmt->fetchAll(PDO::FETCH_OBJ);
			$db = null;
			responseJSON('{"staff":' . json_encode($staffs) . '}',200);
		} catch (PDOException $e) {
			responseJSON('{"error":"text":' . $e->getMessage() . "}}",500);
		}
    }
	
	function getStaff($id) {
        $sql = "select * FROM staff WHERE staff_id=:id";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $id);
			$stmt->execute();
			$staff = $stmt->fetchObject();
			$db = null;
			responseJSON(json_encode($staff),200);
		} catch (PDOException $e) {
			responseJSON('{"error":"text":' . $e->getMessage() . "}}",500);
		}
    }
	
	// curl -i -X POST -H 'Content-Type:application/json' -d '{"lastname":"Cruise", "firstname":"Tom","campus":"Jordanstown"}' http://scm.ulster.ac.uk/~B00697571/rest/api.php/staffs
	
	function addStaff() {
		$request = Slim::getInstance()->request();
		$staff = json_decode($request->getBody());
		
		$sql = "INSERT INTO staff(firstname,lastname,campus) VALUES(:firstname, :lastname, :campus)";
		
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("firstname", $staff->firstname);
			$stmt->bindParam("lastname", $staff->lastname);
			$stmt->bindParam("campus", $staff->campus);
			$stmt->execute();
			
			$staff->Staff_Id = $db->lastInsertId();
			$db = null;
			responseJSON(json_encode($staff),201);
		} catch (PDOException $e) {
			responseJSON('{"error":"text":' . $e->getMessage() . "}}",500);
		} 
		
	}
	
	// curl -i -X PUT -H 'Content-Type:application/json' -d '{"lastname":"Cruise", "firstname":"Tom","campus":"Belfast"}' http://scm.ulster.ac.uk/~B00697571/rest/api.php/staffs/4
	function updateStaff($id) {
		$request = Slim::getInstance()->request();
		$staff = json_decode($request->getBody());
		$sql = "UPDATE staff SET firstname=:firstname, lastname=:lastname, campus=:campus WHERE staff_id=:id";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("firstname", $staff->firstname);
			$stmt->bindParam("lastname", $staff->lastname);
			$stmt->bindParam("campus", $staff->campus);
			$stmt->bindParam("id", $id);
			$stmt->execute();
			
			$db = null;
			responseJSON(json_encode($staff),200);
		} catch (PDOException $e) {
			responseJSON('{"error":"text":' . $e->getMessage() . "}}",500);
		}
	}
	
	// curl -i -X DELETE -H 'Content-Type:application/json' http://scm.ulster.ac.uk/~B00697571/rest/api.php/staffs/4
	function deleteStaff($id) {
		$sql = "DELETE FROM staff WHERE staff_id=:id";
		try {
			$db = getConnection();
			$stmt = $db->prepare($sql);
			$stmt->bindParam("id", $id);
			$staff = $stmt->execute();
			$db = null;
			responseJSON(json_encode($staff),200);
		} catch (PDOException $e) {
			responseJSON('{"error":"text":' . $e->getMessage() . "}}",500);
		}
	}
	
	function getConnection() {
		$dbh = "localhost";
		$dbu = "B00697571";
		$dbp = "EMVXck5s";
		$dbn = $dbu;
		$dbc = new PDO("mysql:host=$dbh;dbname=$dbn",$dbu,$dbp);
		$dbc->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		return $dbc;
	}
	
	function responseJSON($responseBody, $statusCode) {
		$app = Slim::getInstance();
		$response = $app->response();
		$response['Content-Type'] = 'application/json';
		$response->status($statusCode);
		$response->body($responseBody);
	}
?>
