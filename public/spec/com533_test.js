describe("(0+1+...+n)/n",function(){
   it("should return 2 if n is 3", function() {
      var val = calc_func(3);
       expect(val).toEqual(2);
   });
    
    it("should return 'Value not allowed if n is 0'", function() {
       var val = calc_func(0);
        expect(val).toEqual("Value not allowed");
    });
});

processResponse = function(data) { console.log(data); };

describe("Spy on ajax call", function(){
   it("should make an AJAX request with correct setting", function(){
      spyOn($,"ajax");
       getStaff(123,processResponse);
       expect($.ajax).toHaveBeenCalledWith({
          type: 'GET',
           url: "staffs/123",
           dataType: "json",
           success: processResponse
       });
   });
});

describe("Fibnoacci sequence", function() {
   it("should return 0 for fibo(0)", function() {
      expect(fibo(0)).toEqual(0); 
   });
    it("should return 1 for fibo(2)", function() {
      expect(fibo(2)).toEqual(1); 
   });
    it("should return 8 for fibo(6)", function() {
      expect(fibo(6)).toEqual(8); 
   });
});