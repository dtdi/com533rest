function calc_func(n) {
    if(n == 0) 
        return "Value not allowed";
    
    return (n+1)/2;
}

var base_uri="staffs";	
var jsonType="json";
function getStaff(id,callBack) {	
    var requestUrl=base_uri+"/" + id;	
    $.ajax({
        type:       "GET",
        url:        requestUrl,
        dataType:   jsonType,
        success:	callBack
    });	
}

function fibo(n) {
    if ( n == 0 )
        return 0;
    if ( n == 1 )
        return 1;
    
    return fibo(n-1) + fibo(n-2);
}